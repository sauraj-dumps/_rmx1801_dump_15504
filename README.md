## sdm660_64-user 8.1.0 OPM1.171019.011 eng.root.20181228.004601 release-keys
- Manufacturer: oppo
- Platform: sdm660
- Codename: RMX1801
- Brand: 
- Flavor: sdm660_64-user
- Release Version: 8.1.0
- Id: OPM1.171019.011
- Incremental: 1545929312
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: 
- OTA version: 
- Branch: sdm660_64-user-8.1.0-OPM1.171019.011-eng.root.20181228.004601-release-keys
- Repo: _rmx1801_dump_15504


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
